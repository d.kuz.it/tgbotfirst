require('dotenv').config()



const TelegramApi = require('node-telegram-bot-api');
const {gameOption,againOption} = require('./option');
const sequelize = require('./db');
const UserModel = require('./models')
const stick = process.env.STICK
const token = process.env.TOKEN


const bot = new TelegramApi(token, {polling: true})

const chats = {}



const startGame = async  (chatId)=> {
    await bot.sendMessage(chatId, 'угадай число от 1 до 9')
    let randomNumber = String(Math.floor(Math.random() * 10))
    chats[chatId] = randomNumber
    await bot.sendMessage(chatId, 'угадай число',gameOption)
}

const start =  async ()=> {

    try {
        await sequelize.authenticate()
        await sequelize.sync()
    } catch (e) {
        console.log('Connect to BD failure', e)
    }


    bot.setMyCommands([
        {command: '/start', description: 'Приветствие' },
        {command: '/info', description: 'узнать имя и фамилию' },
        {command: '/game', description: 'игра угадай число' },
        // {command: '/again', description: 'играть заного' }
    ])


    bot.on('message', async msg => {
        const text = msg.text
        const chatId = msg.chat.id
        if(text === '/start') {
            const user = await UserModel.findOne({ where: { chatId: chatId.toString() }})
            user ? null : await UserModel.create({chatId})
            await bot.sendSticker(chatId, stick)
            return  bot.sendMessage(chatId, 'Добро пожаловать в бот')
        }


        if(text === '/info'){
            const chatId = msg.chat.id
            const user = await UserModel.findOne({ where: { chatId: chatId.toString() }})
            return await  bot.sendMessage(chatId, `привет тебе ${msg.from.first_name} ${msg.from.last_name === undefined ? '' : msg.from.last_name},
в игре у тебя правильных ответов ${user.right} и ${user.wrong} неправильных ответов`)
        }
        if(text === '/game'){
            return  startGame(chatId)
        }
        return bot.sendMessage(chatId, 'такой команды нет, попробуй еще раз')
    })

    bot.on('callback_query',  async msg =>{
        const data = msg.data;
        const chatId = msg.message.chat.id;
        const msgId = msg.message.message_id
        if(data === '/again'){
            console.log(msgId)
            console.log(chatId)

            return  startGame(chatId)
        }
        const user = await UserModel.findOne({ where: { chatId: chatId.toString() }})
        if(data === chats[chatId]){
            user.right += 1;
             await bot.sendMessage(chatId, `ты угадал цифру ${chats[chatId]}`,againOption)
             await bot.deleteMessage(chatId, msgId)
        } else {
            user.wrong += 1;
             await bot.sendMessage(chatId, `ты не угадал, правильная цифра ${chats[chatId]}`,againOption)
            await bot.deleteMessage(chatId, msgId)
        }
        await user.save()
    })
}
start();